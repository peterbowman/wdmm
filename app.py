# -*- coding: utf-8 -*-

import flask
import license
import re
import toolforge
import wdmm

app = flask.Flask(__name__)
app.before_request(toolforge.redirect_to_https)

toolforge.set_user_agent('wdmm')

ITEM_ID_PATTERN = 'Q[1-9][0-9]{0,9}'

# SELECT ?g WHERE { [] wikibase:wikiGroup ?g. } GROUP BY ?g
# ORDER BY DESC(COUNT(*)) ASC(?g)
WIKI_GROUPS = [
    'wikipedia',
    'wiktionary',
    'wikibooks',
    'wikiquote',
    'wikisource',
    'wikinews',
    'wikivoyage',
    'wikiversity',
    'commons',
    'mediawiki',
    'meta',
    'species',
    'wikidata',
]

PAGES = [
    {
        'itemIds': [
            {'placeholder': 'Q4582194', 'label': 'Item ID'},
            {'placeholder': 'Q16503', 'label': 'Fallback item ID'},
            {'label': 'Alternative fallback item ID'},
        ],
        'legend': 'Send message to this page:',
        'help': u'Fallback item IDs will be used for wikis that don’t have '
                u'a page linked to the first item.',
    },
    {
        'itemIds': [
            {'label': 'Item ID'},
            {'label': 'Fallback item ID'},
            {'label': 'Alternative fallback item ID'},
        ],
        'legend': 'and to this page:',
        'help': u'Feel free to use the same fallbacks here – no page will '
                u'receive the same message more than once.',
    },
    {
        'itemIds': [
            {'label': 'Item ID'},
            {'label': 'Fallback item ID'},
            {'label': 'Alternative fallback item ID'},
        ],
        'legend': 'and this one:',
    },
]


@app.route('/')
@app.route('/index.html')
def index():
    return flask.render_template(
        'index.html',
        itemIdPattern=ITEM_ID_PATTERN,
        wikiGroups=WIKI_GROUPS,
        pages=PAGES,
    )


app.add_url_rule('/license.html', 'licensePage', license.licensePage)


@app.route('/wikidata-massmessage', methods=['POST'])
def getMassMessageFromForm():
    params = flask.request.form

    if 'page1id1' not in params or not params['page1id1']:
        return clientError('You must specify at least one item ID.')

    wikiGroups = []
    if 'wikigroup' in params:
        for wikiGroup in params.getlist('wikigroup'):
            if wikiGroup not in WIKI_GROUPS:
                return clientError('An invalid wiki group was specified.')
            wikiGroups += [wikiGroup]
    else:
        wikiGroups = WIKI_GROUPS

    sitelinks = []
    for pageNum, page in enumerate(PAGES, start=1):
        itemIds = []
        for itemIdNum, itemId in enumerate(page, start=1):
            key = 'page%did%d' % (pageNum, itemIdNum)
            if key in params:
                id = params[key].strip()
                if not id:
                    continue
                if not re.match('^(?:' + ITEM_ID_PATTERN + ')$', id):
                    return clientError('An invalid item ID was specified.')
                useTalk = (key + 'talk') in params
                itemIds += [(id, useTalk)]
        sitelinks += wdmm.getSitelinksForPage(itemIds, wikiGroups)

    massMessage = wdmm.getMassMessage(sorted(set(sitelinks)))
    return flask.Response(massMessage, mimetype='text/plain')


def clientError(message):
    return flask.Response(
        flask.render_template(
            'error.html',
            message=message,
        ),
        status=400
    )
