# -*- coding: utf-8 -*-

import rdflib

SCHEMA = rdflib.namespace.Namespace('http://schema.org/')
WIKIBASE = rdflib.namespace.Namespace('http://wikiba.se/ontology#')


class Site:
    """A site in the Wikimedia Foundation wikifarm."""
    def __init__(self, domain, group):
        self.domain = domain
        self.group = group

    def getApiUrl(self):
        return "https://" + self.domain + "/w/api.php"


class Sitelink:
    """A sitelink of a Wikidata item."""
    def __init__(self, site, title):
        self.site = site
        self.title = title


def getSitelinks(itemId):
    """Load the sitelinks of the given item."""
    uri = rdflib.URIRef('http://www.wikidata.org/entity/' + itemId)
    url = rdflib.URIRef('https://www.wikidata.org/wiki/Special:EntityData/' + itemId + '.ttl')
    sitelinks = []
    g = rdflib.Graph()
    g.parse(url)  # load Turtle URL, not URI via Content Negation, to avoid T252731
    for sitelink in g.subjects(SCHEMA.about, uri):
        siteUri = g.value(sitelink, SCHEMA.isPartOf)
        domain = str(siteUri)[len("https://"):-1]
        group = str(g.value(siteUri, WIKIBASE.wikiGroup))
        site = Site(domain, group)
        title = str(g.value(sitelink, SCHEMA.name))
        sitelinks.append(Sitelink(site, title))
    return sitelinks
