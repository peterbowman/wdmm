# -*- coding: utf-8 -*-

import datetime
import json
import namespaces
import page_data
import prefixed_redis
import redis
import sitelinks
import yaml


def deepUpdate(dict1, dict2):
    for key in dict2:
        if key in dict1 and \
           isinstance(dict1[key], dict) and isinstance(dict2[key], dict):
                deepUpdate(dict1[key], dict2[key])
        else:
            dict1[key] = dict2[key]


with open('config.default.yaml') as configFile:
    config = yaml.safe_load(configFile)
try:
    with open('config.yaml') as localConfigFile:
        localConfig = yaml.safe_load(localConfigFile)
        deepUpdate(config, localConfig)
except FileNotFoundError:
    pass


def dictKeysToInt(dict):
    return {int(key): value for key, value in dict.items()}


cacheConfig = config['redis']
cache = prefixed_redis.PrefixedRedis(cacheConfig['key-prefix'],
                                     host=cacheConfig['host'],
                                     port=cacheConfig['port'])
ttl = datetime.timedelta(days=7)
try:
    cache.info()

    # cache.info() did not crash, we can use the cache
    def getNamespacesForSite(site):
        namespaceInfoStr = cache.get(site.domain)
        if namespaceInfoStr is not None:
            namespaceInfoStr = namespaceInfoStr.decode()
            namespaceInfo = dictKeysToInt(json.loads(namespaceInfoStr))
        else:
            apiUrl = site.getApiUrl()
            namespaceInfo = namespaces.queryNamespaces(apiUrl)
            namespaceInfoStr = json.dumps(namespaceInfo)
            cache.set(site.domain, namespaceInfoStr, ex=ttl)
        return namespaceInfo
except redis.exceptions.ConnectionError:
    # cache not available
    def getNamespacesForSite(site):
        apiUrl = site.getApiUrl()
        return namespaces.queryNamespaces(apiUrl)


def getUnlinkedPages(itemId):
    pages = []
    for (source, title) in page_data.unlinked_pages.items():
        (domain, itemId_) = source.split('|')
        if itemId != itemId_:
            continue
        group = domain.split('.')[1]  # okay since we control unlinked_pages
        site = sitelinks.Site(domain, group)
        pages.append(sitelinks.Sitelink(site, title))
    return pages


def getSitelinksForPage(itemIds, wikiGroups):
    """Get all the sitelinks related to a page.

    A page is specified by a list of one or more item IDs,
    each combined with a bool indicating whether to use the talk page or not
    (as a tuple of string and bool).

    Returns a list of tuples of domain and title."""
    sitelinksForPage = []
    domainsWithSitelink = {}
    for (itemId, useTalk) in itemIds:
        allSitelinks = sitelinks.getSitelinks(itemId)
        allSitelinks += getUnlinkedPages(itemId)
        for sitelink in allSitelinks:
            domain = sitelink.site.domain
            group = sitelink.site.group
            title = sitelink.title
            if group not in wikiGroups:
                continue
            if domain in domainsWithSitelink:
                continue
            domainsWithSitelink[domain] = True
            if domain + '|' + title in page_data.page_overrides:
                title = page_data.page_overrides[domain + '|' + title]
            if useTalk:
                namespaceInfo = getNamespacesForSite(sitelink.site)
                title = namespaces.getTalkPageTitle(namespaceInfo, title)
            sitelinksForPage += [(domain, title)]
            if domain + '|' + title in page_data.extra_pages:
                for extraTitle in page_data.extra_pages[domain + '|' + title]:
                    sitelinksForPage += [(domain, extraTitle)]
    return sitelinksForPage


def getMassMessage(sitelinks):
    """Turn a list of domain-title tuples into a single MassMessage string."""
    massMessage = ''
    for (domain, title) in sitelinks:
        massMessage += u'# {{target|site=%s|page=%s}}\n' % (domain, title)
    return massMessage
