# Wikidata MassMessage

[This tool](https://tools.wmflabs.org/wdmm/) helps community liaisons who need to post messages to the “same” page on many wikis by automating the page name lookup for each wiki.
If a community liaison wants to post to the “village pump” equivalent of each Wiktionary, for example,
they enter the Wikidata item ID of the “village pump” item,
select the `wiktionary` wikigroup,
and then let the tool find all the local page titles on Wikidata and assemble them into a MassMessage blob.
The result of the tool can be used directly with the [MassMessage](https://www.mediawiki.org/wiki/Extension:MassMessage) extension.

## Toolforge setup

On Wikimedia Toolforge, this tool runs under the `wdmm` tool name.
Source code resides in `~/www/python/src/`,
a virtual environment is set up in `~/www/python/venv/`,
logs end up in `~/uwsgi.log`.

If the web service is not running for some reason, run the following command:
```
webservice start
```
If it’s acting up, try the same command with `restart` instead of `start`.
Both should pull their config from the `service.template` file,
which is symlinked from the source code directory into the tool home directory.

To update the service, run the following commands:
```
source ~/www/python/venv/bin/activate
pip3 install -r requirements.txt
webservice restart
```

## Local development setup

```
git clone https://gitlab.wikimedia.org/toolforge-repos/wdmm.git
cd wdmm
pip3 install -r requirements.txt
FLASK_APP=app.py FLASK_DEBUG=1 flask run
```

If you want, you can do this inside some virtualenv too.

## License

The content of this repository is released under the AGPL v3 as provided in the LICENSE file that accompanied this code.

By submitting a “pull request” or otherwise contributing to this repository, you agree to license your contribution under the license mentioned above.
