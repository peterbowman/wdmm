# -*- coding: utf-8 -*-

import redis


class PrefixedRedis(redis.StrictRedis):
    def __init__(self, prefix, **kwargs):
        self.prefix = prefix
        super().__init__(**kwargs)

    def get(self, key, **kwargs):
        return super().get(self.prefix + key, **kwargs)

    def set(self, key, value, **kwargs):
        return super().set(self.prefix + key, value, **kwargs)
