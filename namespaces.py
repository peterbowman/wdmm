# -*- coding: utf-8 -*-

import requests


def queryNamespaces(apiEndpoint):
    """Get the map from namespace IDs to names from a given API endpoint.

    For each namespace ID, a list of names is returned.
    The first entry is the local name,
    the second entry is the canonical name,
    and the remaining entries (if any) are further aliases."""
    namespaces = {}
    r = requests.get(apiEndpoint,
                     headers={
                         'Accept': 'application/json',
                     },
                     params={
                         'action': 'query',
                         'meta': 'siteinfo',
                         'siprop': 'namespaces|namespacealiases',
                         'format': 'json',
                         'formatversion': 2,
                     })
    responseJson = r.json()['query']
    namespacesJson = responseJson['namespaces']
    for id in namespacesJson:
        namespace = namespacesJson[id]
        id = namespace['id']
        localName = namespace['name']
        if 'canonical' in namespace:
            canonicalName = namespace['canonical']
        else:
            canonicalName = ''
        namespaces[id] = [localName, canonicalName]
    namespaceAliasesJson = responseJson['namespacealiases']
    for namespaceAliasJson in namespaceAliasesJson:
        id = namespaceAliasJson['id']
        names = namespaceAliasJson['alias'].split(' ')
        namespaces[id] += names
    return namespaces


def splitTitle(title):
    """Split a namespaced title into namespace name and title text."""
    split = title.split(':', maxsplit=1)
    if len(split) > 1:
        return (split[0], split[1])
    else:
        return ('', title)


def getNamespaceIdByName(namespaces, name):
    """Look up the ID for a namespace name."""
    for id in namespaces:
        if name in namespaces[id]:
            return id
    return None


def getNamespaceNameById(namespaces, id):
    """Look up the local name for a namespace ID."""
    if id in namespaces:
        return namespaces[id][0]
    else:
        return None


def getTalkPageTitle(namespaces, title):
    """Get the title of the talk page associated with the given title."""
    (topicNamespaceName, titleText) = splitTitle(title)
    topicNamespaceId = getNamespaceIdByName(namespaces, topicNamespaceName)
    if topicNamespaceId is None:
        # this is actually a main namespace page with a colon in the title
        topicNamespaceId = 0
    talkNamespaceId = topicNamespaceId + 1
    talkNamespaceName = getNamespaceNameById(namespaces, talkNamespaceId)
    return talkNamespaceName + ':' + titleText
